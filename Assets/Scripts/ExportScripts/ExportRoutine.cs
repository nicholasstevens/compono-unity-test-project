﻿using UnityEngine;
using UnityEngine.AddressableAssets;

public class ExportRoutine : MonoBehaviour
{   
    [SerializeField] private SpawnItemList m_itemList = null;

    private AssetReferenceGameObject m_assetLoadedAsset;
    private GameObject m_instanceObject = null;

    private void Start()
    {
        if (m_itemList == null || m_itemList.AssetReferenceCount == 0) 
        {
            Debug.LogError("Spawn list not setup correctly");
        }        
        LoadItemAtIndex(m_itemList, 0);
    }

    private void LoadItemAtIndex(SpawnItemList itemList, int index) 
    {
        if (m_instanceObject != null) 
        {
            Destroy(m_instanceObject);
        }       
        
        m_assetLoadedAsset = itemList.GetAssetReferenceAtIndex(index);
        var spawnPosition = new Vector3();
        var spawnRotation = Quaternion.identity;
        var parentTransform = this.transform;


        var loadRoutine = m_assetLoadedAsset.LoadAssetAsync();
        loadRoutine.Completed += LoadRoutine_Completed;

        void LoadRoutine_Completed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
        {
            m_instanceObject = Instantiate(obj.Result, spawnPosition, spawnRotation, parentTransform);

            // code below this line is added code
            
            // store the asset instance transform
            m_instanceTransform = m_instanceObject.transform;

            // set the loaded index
            m_loadedIndex = index;

            // begin the export routine
            StartCoroutine(ExportInstance());
        }
    }

    // index of currently loaded asset
    private int m_loadedIndex = -1;

    // the transform of the loaded asset instance
    private Transform m_instanceTransform;

    // the rotation to apply between every screenshot on export
    [SerializeField] private Vector3 m_eulerRotationPerStep = new Vector3(0.0f, 22.5f, 0.0f);

    // the coordinate space of the rotation to apply
    [SerializeField] private Space m_rotationSpace = Space.World;

    // the number of rotations to apply
    [SerializeField] private int m_rotationSteps = 16;

    // the texture used to read the pixels of the screen
    private Texture2D m_screenTexture = null;

    // the folder to save the screenshots to (Project Folder/Screenshots/)
    private string screenshotFolder = null;

    // used to initialize screenshot directory
	private void Awake()
	{
        // Application.dataPath is the (Project Folder/Assets) directory
        screenshotFolder = System.IO.Path.Combine(Application.dataPath, "..", "Screenshots");

        // ensure the directory exists before export
        System.IO.Directory.CreateDirectory(screenshotFolder);
    }

    /// <summary>
    /// Load the next asset in the item list.
    /// A new asset will not be loaded if we are at the end of the list.
    /// </summary>
    /// <returns>If a new asset was loaded.</returns>
    private bool LoadNextAsset()
	{
        int nextIndex = m_loadedIndex + 1;
        bool hasNextAsset = nextIndex < m_itemList.AssetReferenceCount;

        if (hasNextAsset)
            LoadItemAtIndex(m_itemList, nextIndex);

        return hasNextAsset;
	}

    /// <summary>Rotates the existing asset instance using the given rotation and space.</summary>
    /// <param name="eulers">The euler rotation values.</param>
    /// <param name="relativeTo">In what coordinate space to rotate the asset instance.</param>
    private void RotateAssetInstance(Vector3 eulers, Space relativeTo)
	{
        // an asset instance is loaded and it's transform has been stored
        if (m_instanceTransform != null)
            m_instanceTransform.Rotate(eulers, relativeTo);
	}

    /// <summary>Saves a screenshot to the given file path.</summary>
    /// <param name="outputFile">The path to where the file will be saved.</param>
    private void SaveScreenshot(string outputFile)
    {
        // read the pixels from the screen into the texture
        m_screenTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        m_screenTexture.Apply();

        // save the texture as a png to the screenshot directory
        byte[] data = m_screenTexture.EncodeToPNG();
        System.IO.File.WriteAllBytes(outputFile, data);
    }

    /// <summary>Exports the current loaded asset instance.</summary>
    private System.Collections.IEnumerator ExportInstance()
	{
        // (re)create screen texture
        if (m_screenTexture == null)
            m_screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        else if (m_screenTexture.width != Screen.width || m_screenTexture.height != Screen.height)
		{
            Destroy(m_screenTexture);
            m_screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        }

        string outputName = System.IO.Path.Combine(screenshotFolder, m_assetLoadedAsset.Asset.name);

        for (int i = 0; i < m_rotationSteps; i++)
		{
            string outputFile = $"{outputName} {i}.png";

            // wait for end of frame when cameras have finished rendering
            yield return new WaitForEndOfFrame();

            SaveScreenshot(outputFile);

            // rotate the asset instance for the next screenshot
            RotateAssetInstance(m_eulerRotationPerStep, m_rotationSpace);
		}

        // load the next asset if there is one
        if (!LoadNextAsset())
        {
            // free screen texture if it is no longer needed
            Destroy(m_screenTexture);
            m_screenTexture = null;
        }

        yield return null;
	}
}
